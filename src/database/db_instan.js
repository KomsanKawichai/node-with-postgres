const Sequelize = require("sequelize");
const sequelize = new Sequelize("demo", "postgres", "123456", {
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    idle: 10000,
  },
});

// var sequelize = new Sequelize('postgres://user:pass@example.com:5432/dbname');

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require("../models/user")(sequelize, Sequelize);
// db.registercar = require("../models/user-car")(sequelize, Sequelize);
// db.parkingofcar = require("../models/parking-of-car")(sequelize, Sequelize);

// db.parkingofcar.belongsTo(db.registercar);
// db.parkingofcar.belongsTo(db.parkinglot);

db.users.sync({ force: false });
// db.registercar.sync({ force: false });
// db.parkingofcar.sync({ force: false });

module.exports = db;
