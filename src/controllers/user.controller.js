const db = require("../database/db_instan");

module.exports = {
  findAllUser: async (ctx, next) => {
    try {
      console.log("findAll");
      const findUser = await db.users.findAll();
      ctx.status = 200;
      return (ctx.body = {
        result: true,
        data: findUser,
      });
    } catch (error) {
      console.log("error ", error);
      ctx.status = 500;
      ctx.body = {
        result: false,
        errorMsg: "please contact admin",
        errorCode: error,
      };

      return next();
    }
  },

  findByIdUser: async (ctx, next) => {
    try {
      console.log("findByIdUser ", ctx.params.id);
      const findById = ctx.params.id;
      const findByIdUser = await db.users.findOne({
        where: {
          Id: findById,
        },
      });

      ctx.status = 200;
      if (!findByIdUser) {
        return (ctx.body = {
          result: false,
          msg: "Notfound user",
        });
      }

      return (ctx.body = {
        result: true,
        data: findByIdUser,
      });
    } catch (error) {
      console.log("error ", error);
      ctx.status = 500;
      ctx.body = {
        result: false,
        errorMsg: "please contact admin",
        errorCode: error,
      };

      return next();
    }
  },

  creatUser: async (ctx, next) => {
    try {
      console.log("creatUser ", ctx.request.body);
      const { firstname, lastname, age, email } = ctx.request.body;

      const checkUser = await db.users.findOne({
        where: {
          firstname: firstname,
          lastname: lastname,
        },
      });

      ctx.status = 200;
      if (checkUser) {
        return (ctx.body = {
          result: false,
          msg: "firstname and lastname already exist",
        });
      }

      const createUser = await db.users.create({
        firstname: firstname,
        lastname: lastname,
        age: age,
        email: email,
      });

      return (ctx.body = {
        result: true,
        data: createUser,
      });
    } catch (error) {
      console.log("error ", error);
      ctx.status = 500;
      ctx.body = {
        result: false,
        errorMsg: "please contact admin",
        errorCode: error,
      };
      return next();
    }
  },

  updateUser: async (ctx, next) => {
    try {
      console.log("updateUser ", ctx.request.body);
      const { firstname, lastname, age, email, id } = ctx.request.body;

      const findUserForUpdate = await db.users.findOne({
        where: {
          Id: id,
        },
      });

      ctx.status = 200;
      if (!findUserForUpdate) {
        return (ctx.body = {
          result: false,
          msg: "Notfound user",
        });
      }

      const updateUser = await db.users.update(
        {
          firstname: firstname,
          lastname: lastname,
          age: age,
          email: email,
        },
        { where: { Id: id } }
      );

      return (ctx.body = {
        result: true,
      });
    } catch (error) {
      console.log("error ", error);
      ctx.status = 500;
      ctx.body = {
        result: false,
        errorMsg: "please contact admin",
        errorCode: error,
      };
      return next();
    }
  },

  deleteUser: async (ctx, next) => {
    try {
      const { id } = ctx.request.body;

      const findUserForDelete = await db.users.findOne({
        where: {
          Id: id,
        },
      });

      ctx.status = 200;
      if (!findUserForDelete) {
        return (ctx.body = {
          result: false,
          msg: "Notfound user",
        });
      }

      const deleteUser = await db.users.destroy({ where: { Id: id } });

      return (ctx.body = {
        result: true,
      });
    } catch (error) {
      console.log("error ", error);
      ctx.status = 500;
      ctx.body = {
        result: false,
        errorMsg: "please contact admin",
        errorCode: error,
      };
      return next();
    }
  },
};
