const Router = require("koa-router");
const userController = require("../controllers/user.controller");
const router = new Router();

router.get("/", (ctx, next) => {
  ctx.status = 200;
  ctx.body = { msg: "Hello World" };
});

router.get("/user", userController.findAllUser);
router.get("/user/:id", userController.findByIdUser);
router.post("/user", userController.creatUser);
router.put("/user", userController.updateUser);
router.delete("/user", userController.deleteUser);

module.exports = router;
