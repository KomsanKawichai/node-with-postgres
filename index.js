const bodyParser = require("koa-bodyparser");
const Koa = require("koa");
const indexRoute = require("./src/routes/index");
const app = new Koa();
app.use(bodyParser());
app.use(indexRoute.routes());

app.listen("3000").on("error", (err) => {
  console.log("err : ", err);
});
